from setuptools import setup

setup(name='friendbot',
      version='0.0.1',
      description='A friend bot',
      author='Aloha',
      author_email='thomas@aloha.im',
      license='MIT',
      packages=['friendbot'],
      install_requires=[
        'sleekxmpp',
      ],
      scripts=[
          'bin/friend-bot'
      ]
)

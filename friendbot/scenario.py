import logging
from sleekxmpp.xmlstream import JID

logger = logging.getLogger(__name__)


class Scenario:

    def __init__(self):
        self._current_event = None
        self._current_user = None
        self._current_message = None

    @property
    def current_event(self):
        return self._current_event

    @current_event.setter
    def current_event(self, value):
        self._current_event = value
        self._current_user = JID(jid=self._current_event['from'])
        self._current_message = str(self._current_event['body']).strip()

    def log(self, message, level=logging.DEBUG):
        """Enregistre un log"""

        message = "[{}] {}".format(self.__class__.__name__, message)
        logger.log(level=level, msg=message)

    def can_handle(self):
        return False

    def handle(self):
        return ""


class GreetingScenario(Scenario):

    def can_handle(self):
        assert self._current_event

        msg = self._current_message.lower()
        greetings = ['salut', 'coucou', 'bonjour', 'yo', 'plop']

        for greeting in greetings:
            if greeting in msg:
                return True

    def handle(self):
        self.log(self._current_user)
        return "Bonjour {}".format(self._current_user)


class FallbackScenario(Scenario):

    def can_handle(self):
        return True

    def handle(self):
        return "Je ne sais quoi répondre..."

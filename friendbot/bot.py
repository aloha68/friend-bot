import logging
import sleekxmpp
from .scenario import *

logger = logging.getLogger(__name__)


class FriendBot(sleekxmpp.ClientXMPP):

    def __init__(self, username, password, server, resource):
        """
        Innitialise le bot
        :param username:    nom du bot
        :param password:    password du bot
        :param server:      serveur xmpp
        :param resource:    ressource xmpp
        """

        jid = "{}@{}/{}".format(username, server, resource)
        logger.debug("Creating FriendBot with jid: {}".format(jid))

        super().__init__(jid=jid, password=password)

        self.register_plugin('xep_0030') # Service discovery
        self.register_plugin('xep_0199') # XMPP Ping

        self.add_event_handler('session_start', pointer=self.on_session_start)
        self.add_event_handler('message', pointer=self.on_message)

        # Liste des scénarios supportés par le bot
        self.scenarios = [
            GreetingScenario(),
            FallbackScenario()
        ]
        self.last_scenario_used = None

    @property
    def jid(self):
        return self.boundjid.full

    @property
    def username(self):
        return self.boundjid.username

    def connect(self, *args, **kwargs):
        """Lancement du bot"""

        if not super().connect(*args, **kwargs):
            logger.error("{} not enable to connect XMPP".format(self.username))
            return

        self.process(threaded=True)

    def disconnect(self, *args, **kwargs):
        """Extinction du bot"""

        super().disconnect(*args, **kwargs)
        logger.debug("{} disconnected!".format(self.username))

    def on_session_start(self, ev):
        """Événement appelé lors de la connexion au serveur"""

        self.get_roster()
        self.send_presence()

        logger.debug('{} connected as {}'.format(self.username, self.jid))

    def on_message(self, ev):
        """Événement appelé lors d'un nouveau message"""

        if ev['type'] != 'chat':
            return

        user = ev['from']
        msg = str(ev['body']).strip()

        logger.debug("{} got message from {}: {}".format(self.username, user, msg))

        for scenario in self.scenarios:
            if self.try_scenario(scenario, event=ev):
                return

        logger.debug("{} can't answer".format(self.username))

    def try_scenario(self, scenario, event):
        """Essaie de répondre à un événement grâce à un scénario"""

        scenario.current_event = event
        if scenario.can_handle():
            msg = scenario.handle()
            if msg:
                event.reply(msg).send()
                logger.debug("{} answer: {}".format(self.username, msg))
                return True

        return False

# XMPP configuration
# Your JID will be:
#   XMPP_BOT_ID@XMPP_SERVER/XMPP_BOT_RESOURCE
#
XMPP_USER = ''
XMPP_SERVER = ''
XMPP_PASSWORD = ''
XMPP_RESOURCE = 'FriendBot'
# Administrator of your xmpp server
XMPP_ADMIN = ''

# Try to override settings
try:
    from local_settings import *
except ImportError:
    pass
from friendbot import FriendBot
import logging
import settings

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("sleekxmpp").setLevel(logging.WARNING)
logging.getLogger("sleekxmpp.xmlstream.cert").setLevel(logging.ERROR)


if __name__ == '__main__':
    bot = FriendBot(
        username=settings.XMPP_USER,
        password=settings.XMPP_PASSWORD,
        server=settings.XMPP_SERVER,
        resource=settings.XMPP_RESOURCE
    )

    while True:

        cmd = input("Votre message: ")
        if not cmd or cmd == 'quit':
            break

        tmp = {
            'from': settings.XMPP_ADMIN,
            'body': cmd
        }

        for scenario in bot.scenarios:
            scenario.current_event = tmp
            if scenario.can_handle():
                msg = scenario.handle()
                if msg:
                    print(msg)
                    break
